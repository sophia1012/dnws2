using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Collections;
using DNWS1;

namespace DNWS
{
    // Main class
    public class Program
    {
        static public IConfigurationRoot Configuration { get; set; }

        // Log to console
        public void Log(String msg)
        {
            Console.WriteLine(msg);
        }

        // Start the server, Singleton here
        public void Start()
        {
            // Start server
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("config.json");
            Configuration = builder.Build();
            DotNetWebServer ws = DotNetWebServer.GetInstance(Convert.ToInt16(Configuration["Port"]), this);
            ws.Start();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.Start();
        }
    }

    /// <summary>
    /// HTTP processor will process each http request
    /// </summary>

    public class HTTPProcessor
    {
        // Get config from config manager, e.g., document root and port
        protected string ROOT = Program.Configuration["DocumentRoot"];
        protected Socket _client;
        protected Program _parent;
        protected static Dictionary<String, int> statDictionary = null;
        private int count = Convert.ToInt32(Program.Configuration["Thread"]);
        protected StringBuilder sb = new StringBuilder();
        /// <summary>
        /// Constructor, set the client socket and parent ref, also init stat hash
        /// </summary>
        /// <param name="client">Client socket</param>
        /// <param name="parent">Parent ref</param>
        public HTTPProcessor(Socket client, Program parent)
        {
            _client = client;
            _parent = parent;
            if (statDictionary == null)
            {
                statDictionary = new Dictionary<String, int>();

            }
        }

        /// <summary>
        /// Increment counting for the url
        /// </summary>
        /// <param name="url">target url</param>
        public void incr(string url)
        {
            if (statDictionary.ContainsKey(url))
            {
                statDictionary[url] = (int)statDictionary[url] + 1;
            }
            else
            {
                statDictionary[url] = 1;
            }
        }

        /// <summary>
        /// Gen HTML stat table
        /// </summary>
        /// <returns>HTML stat table</returns>
        public string genStat()
        {
            WaitCallback callBack;
            sb.Append("<html><body>");
            sb.Append("<p>Size of Threadpool : " + (int)count + "</p>");
            callBack = new WaitCallback(ThreadProcess);
            for (int i = 0; i < count; i++)
            {
                ThreadPool.QueueUserWorkItem(callBack, i);
            }
            Thread.Sleep(2000);
            sb.Append("</html></body>");
            return sb.ToString();
        }

        /// <summary>
        /// Gen HTML about client info using information from request header
        /// </summary>
        /// <returns>HTML client info</returns>
        public void ThreadProcess(Object state)
        {
            sb.Append("<p>Thread #" + (int)state + "</p>");
            sb.Append("<p>Request processed</p>");
        }

        /// <summary>
        /// Get a file from local harddisk based on path
        /// </summary>
        /// <param name="path">Absolute path to the file</param>
        /// <returns></returns>
        protected HTTPResponse getFile(String path)
        {
            HTTPResponse response = null;

            // Guess the content type from file extension
            string fileType = "text/html";
            if (path.ToLower().EndsWith("jpg") || path.ToLower().EndsWith("jpeg"))
            {
                fileType = "image/jpeg";
            }
            if (path.ToLower().EndsWith("png"))
            {
                fileType = "image/png";
            }

            // Try to read the file, if not found then 404, otherwise, 500.
            try
            {
                response = new HTTPResponse(200);
                response.type = fileType;
                response.body = System.IO.File.ReadAllBytes(path);
            }
            catch (FileNotFoundException ex)
            {
                response = new HTTPResponse(404);
                response.body = Encoding.UTF8.GetBytes("<h1>404 Not found</h1>" + ex.Message);
            }
            catch (Exception ex)
            {
                response = new HTTPResponse(500);
                response.body = Encoding.UTF8.GetBytes("<h1>500 Internal Server Error</h1>" + ex.Message);
            }
            return response;

        }

        /// <summary>
        /// Get a request from client, process it, then return response to client
        /// </summary>
        public void Process()
        {
            NetworkStream ns = new NetworkStream(_client);
            string request = "";
            HTTPResponse response = null;
            byte[] bytes = new byte[1024];
            int bytesRead;

            // Read all request
            do
            {
                bytesRead = ns.Read(bytes, 0, bytes.Length);
                request += Encoding.UTF8.GetString(bytes);
            } while (ns.DataAvailable);

            // We can handle only GET now
            String[] tokens = Regex.Split(request, "\\s");
            if (tokens.Length < 1)
            {
                response = new HTTPResponse(500);
            }
            else
            {
                if (tokens.Length < 2)
                {
                    response = new HTTPResponse(500);
                }
                else if (!tokens[0].ToLower().Equals("get"))
                {
                    response = new HTTPResponse(501);
                }
                else
                {
                    String[] subTokens = Regex.Split(tokens[1], "/");
                    // CGI?
                    if (tokens[1].ToLower().EndsWith("/stat"))
                    {
                        response = new HTTPResponse(200);
                        response.body = Encoding.UTF8.GetBytes(genStat());
                    }
                    // client info?

                    // GAME?
                    else if (subTokens[subTokens.Length - 1].ToLower().StartsWith("ox"))
                    {
                        OXGame game = OXGame.GetInstance();
                        response = game.Process(subTokens[subTokens.Length - 1]);
                    }
                    // Then it's a file request
                    else
                    {
                        if (tokens[1].EndsWith("/"))
                        {
                            response = getFile(ROOT + "/index.html");
                        }
                        else
                        {
                            response = getFile(ROOT + tokens[1]);
                        }
                        incr(tokens[1]);
                    }
                }
            }


            // For testing purpose
            // Thread.Sleep(1000);
            // Generate response
            ns.Write(Encoding.UTF8.GetBytes(response.header), 0, response.header.Length);
            if (response.body != null)
            {
                ns.Write(response.body, 0, response.body.Length);
            }

            // Shuting down
            //ns.Close();
            _client.Shutdown(SocketShutdown.Both);
            //_client.Close();

        }
    }

    /// <summary>
    /// Main server class, open the socket and wait for client
    /// </summary>
    public class DotNetWebServer
    {
        protected int _port;
        protected Program _parent;
        protected Socket serverSocket;
        protected Socket clientSocket;
        private static DotNetWebServer _instance = null;
        protected int id;

        private DotNetWebServer(int port, Program parent)
        {
            _port = port;
            _parent = parent;
            id = 0;
        }

        /// <summary>
        /// Singleton here
        /// </summary>
        /// <param name="port">Listening port</param>
        /// <param name="parent">parent ref</param>
        /// <returns></returns>
        public static DotNetWebServer GetInstance(int port, Program parent)
        {
            if (_instance == null)
            {
                _instance = new DotNetWebServer(port, parent);
            }
            return _instance;
        }

        /// <summary>
        /// Server starting point
        /// </summary>
        public void Start()
        {
            try
            {
                // Create listening socket, queue size is 5 now.
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, _port);
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(localEndPoint);
                serverSocket.Listen(5);
                _parent.Log("Server started at port " + _port + ".");
            }
            catch (Exception ex)
            {
                _parent.Log("Server started unsuccessfully.");
                _parent.Log(ex.Message);
                return;
            }
            while (true)
            {
                try
                {
                    // Wait for client
                    clientSocket = serverSocket.Accept();
                    // Get one, show some info
                    _parent.Log("Client accepted:" + clientSocket.RemoteEndPoint.ToString());
                    HTTPProcessor hp = new HTTPProcessor(clientSocket, _parent);
                    // Single thread
                    //hp.Process();
                    // End single therad

                    // Multi thread
                    Thread Thread = new Thread(new ThreadStart(hp.Process));
                    Thread.Name = "id:" + id++;
                    //_parent.Log("Start thread with name " + Thread.Name);
                    Thread.Start();
                    // End multi thread
                }
                catch (Exception ex)
                {
                    _parent.Log("Server starting error: " + ex.Message + "\n" + ex.StackTrace);

                }
            }

        }
    }
}
//i